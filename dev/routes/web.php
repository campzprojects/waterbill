<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/payments/form', 'PaymentController@index')->name('payment_form');
Route::post('/payments', 'PaymentController@savePayment')->name('payment');

Auth::routes();

//bill officer
Route::get('/bill/bill-officer/mark', 'BillController@markBill');
Route::post('/customer/get/data', 'BillController@getCustomer');
Route::post('/bill/bill-officer/save', 'BillController@saveCustomer')->name('billsave');
Route::post('/bill-officer/calc/', 'BillController@calc');


//customer
Route::get('/profile', 'CustomerController@profile')->name('profile');
Route::get('/profile/last-bill', 'CustomerController@lastBill')->name('lastbill');
Route::get('/profile/billing-history', 'CustomerController@billingHistory');
Route::get('/profile/payment-history', 'CustomerController@paymentHistory');
Route::post('/profile/update', 'CustomerController@profileUpdate')->name('profileUpdate');

//Route::get('/profile/edit', 'CustomerController@profileedit');
Route::get('/invoice/view/{id}', 'CustomerController@viewInvoice');

//managers
//bill-officer
Route::get('/manager/bill-officer/list', 'ManagerController@listBillOfficers');
Route::get('/manager/bill-officer/delete/{id}', 'ManagerController@deleteBillOfficers');
Route::get('/manager/bill-officer/deactive/{id}', 'ManagerController@deactiveBillOfficers');
Route::get('/manager/bill-officer/active/{id}', 'ManagerController@activeBillOfficers');



Route::get('/manager/bill-officer/add', 'ManagerController@addBillOfficers');
Route::post('/manager/bill-officer/save', 'ManagerController@saveBillOfficers')->name('savebillofficer');

//customer-support
Route::get('/manager/customer-support/list', 'ManagerController@listCustomerSupports');
Route::get('/manager/customer-support/delete/{id}', 'ManagerController@deleteCustomerSupports');
Route::get('/manager/customer-support/deactive/{id}', 'ManagerController@deactiveCustomerSupports');
Route::get('/manager/customer-support/active/{id}', 'ManagerController@activeCustomerSupports');

Route::get('/manager/customer-support/add', 'ManagerController@addCustomerSupports');
Route::post('/manager/customer-support/save', 'ManagerController@saveCustomerSupports')->name('saveCustomerSupport');

//customer support oficer
Route::get('/customer-support/customers/list', 'CustomerSupportController@listCustomers');
Route::get('/customer-support/payments/list', 'CustomerSupportController@listPayments');
Route::get('/customer-support/customers/bills/all', 'CustomerSupportController@listAllCustomersBills');
Route::get('/customer-support/customers/bills/settled', 'CustomerSupportController@listSettledCustomersBills');
Route::get('/customer-support/customers/bills/not-settled', 'CustomerSupportController@listNotSettledCustomersBills');
Route::get('/customer-support/customers/bills/red-notice/{id}/{date}', 'CustomerSupportController@redNoticeSend');
Route::get('/customer-support/customers/bills/red-notice-remove/{id}/', 'CustomerSupportController@removeRedNoticeSend');
Route::get('/profile/edit', 'CustomerController@profileEdit');
Route::post('/profile/update-profile', 'CustomerController@update_profile')->name('update-profile');
Route::get('/invoice/view/{id}', 'CustomerController@viewInvoice');
