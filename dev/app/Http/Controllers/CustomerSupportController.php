<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\Payment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerSupportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   public function listCustomers(){
       $users = User::where('role','customer')->get();
       return view('customer-support.list-customers')
           ->with('users',$users);
   }

   public function listPayments(){
       $payments = Payment::get();
        return view('customer-support.list-payments')
            ->with('payments',$payments);
   }
   public function listAllCustomersBills(){
       $bills = Bill::get();
        return view('customer-support.list-customers-bills')
            ->with('bills',$bills);
   }
   public function listSettledCustomersBills(){
       $bills = Bill::where('is_settle',1)->get();
        return view('customer-support.list-customers-bills')
            ->with('bills',$bills);
   }
    public function listNotSettledCustomersBills(){
        $bills = Bill::where('is_settle',0)->get();
        return view('customer-support.list-customers-bills')
            ->with('bills',$bills);
    }

   public function redNoticeSend($id,$date){
       $issettled = Bill::where('id',$id)->first();
       if ($issettled->is_settle){
           return redirect()->back()->with('error', 'Bill alredy Settled Added!');
       }
        $bill = Bill::where('id',$id)->whereNull('red_notice')->first();
//        dd($bill);
        if ($bill){
            $bill->red_notice = Carbon::parse($date)->format('Y-m-d');
            $bill->red_notice_due = Carbon::parse($date)->addDays(3)->format('Y-m-d');
            $res = $bill->save();
            if ($res){
                return redirect()->back()->with('success', 'Red notice Added Success!');
            }else{
                return redirect()->back()->with('error', 'Red notice not Added!');
            }
        }else{
            return redirect()->back()->with('error', 'Red notice already Added!');
        }
   }

   public function removeRedNoticeSend($id){
       $bill = Bill::where('id',$id)->update(array('red_notice' => null, 'red_notice_due'=>null));
       return redirect()->back()->with('success', 'Red notice remove Success!');
   }
}
