<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function lastBill(){
        $user = Auth::user();
//        dd($user);
        $account_no = '';
        $amount=0;
        $invoice_no='';
        $month='';
        $bill_id='';
        $payment_histories = Bill::where('account_no',Auth::user()->account_no)->orderBy('created_at','DESC')->first();
// $user = User::where('account_no',$payment_histories->account_no)->where('status',1)->first();
     //  dd($payment_histories);
        if ($payment_histories){
            $account_no = $user->account_no;
            $amount=$user->due_previous_month;
            $invoice_no=$payment_histories->invoice_no;
            $month=$payment_histories->month;
            $bill_id=$payment_histories->id;
        }

        return view('customer.last-bill')
            ->with('bill_info',$payment_histories)
            ->with('user',$user)
            ->with('account_no',$account_no)
            ->with('amount',$amount)
            ->with('invoice_no',$invoice_no)
            ->with('month',$month)
            ->with('bill_id',$bill_id);

    }

    public function billingHistory(){
        $payment_histories = Bill::where('account_no',Auth::user()->account_no)->orderBy('created_at','DESC')->get();

        return view('customer.billing_history')->with('payment_histories',$payment_histories);
    }
    public function paymentHistory(){
        $payment_histories = Payment::where('account_no',Auth::user()->account_no)->orderBy('created_at','DESC')->get();
//dd($payment_histories);
        return view('customer.payment_history')->with('payment_histories',$payment_histories);
    }
    public function profile(){

        return view('customer.profile');
    }
    public function profileUpdate(Request $request){
//dd($request->all());
        return view('customer.profile');
    }
    public function profileEdit(){
        return view('customer.profileedit');
    }
    public function update_profile(Request $request ){
        $profile = User::where('id',$request->id)->first();
        $profile->name = $request->name;
        $profile->email = $request->email;
        $profile->password = $request->password;
        $profile->account_no = $request->account_no;
        $profile->company = $request->company;
        $profile->nic = $request->nic;
        $profile->address = $request->address;
        $profile->payment_method = $request->payment_method;
        $profile->meter_no = $request->meter_no;
        if(!$request->password){
            $profile->password =  $profile->password;
        }
        else{
            $profile->password = bcrypt($request->password);
        }
        $a=$profile->save();
        if($a){
            return redirect()->back()->with('success', 'Your Profile Updated');
        }
        else{
            return redirect()->back()->with('error', 'Your Profile not Updated ');
        }


    }

    public function viewInvoice($id){
        $bill_info = Bill::where('id',$id)->first();
        if (!$bill_info){
            abort(404);
        }
        $user = User::where('account_no',$bill_info->account_no)->where('status',1)->first();
//        dd($user);
        return view('customer.invoice')
            ->with('bill_info',$bill_info)
            ->with('user',$user);
    }

}
