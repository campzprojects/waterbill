<?php

namespace App\Http\Controllers;

use App\Models\bill;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BillController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function markBill()
    {
        $users = User::where('role', 'customer')->where('status', 1)->get();
        return view('bill.billofficer')->with('users', $users);
    }

    public function getCustomer(Request $request)
    {
        $user = User::where('account_no', $request->account_no)
            ->where('status', 1)->first();

        $last_bill = Bill::where('account_no',$request->account_no)->orderBy('created_at','desc')->first();

        return response()->json(['user' => $user, 'invoice_no' => $this->generateInvoiceNo(),'last_bill'=>$last_bill]);
    }

    public function saveCustomer(Request $request)
    {
//        dd($request->all());
        $ext = Bill::where('account_no', $request->account_no)->where('month', $request->month)->count();
        if ($ext) {
            return redirect()->back()->with('error', 'Bill already exist for this customer!');
        }
//        dd('aa');
        $res = new Bill($request->except('_token'));
        $a = $res->save();
        if ($a) {
            $user = User::where('account_no', $request->account_no)->where('status', 1)->first();
            $user->due_previous_month = $request->total_amount;
            $user->save();
            return redirect()->back()->with('success', 'Customer Bill saved');
        } else {
            return redirect()->back()->with('error', 'Customer Bill not saved');

        }
    }

    private function generateInvoiceNo()
    {
        return 'in_' . rand(100, 10000) . Carbon::today()->format('md');
    }


    public function calc(Request $request)
    {
        $units = $request->units;
        $service_charge = 50;
        $unit_total = 0;
        $unit_price = 5;

        if ($units <= 5) {
            $service_charge = 50;
            $unit_price = 5;
            $unit_total = $units * $unit_price;

        } else if ($units >= 6 AND $units <= 10) {
            $service_charge = 50;
            $unit_price = 10;
            $unit_total = (5 * 5) + ($units - 5) * $unit_price;


        } else if ($units >= 11 AND $units <= 15) {
            $service_charge = 50;
            $unit_price = 15;
            $unit_total = (5 * 5) + (5 * 10) + ($units - 10) * $unit_price;

        } else if ($units >= 16 AND $units <= 20) {
            $service_charge = 80;
            $unit_price = 40;
            $unit_total = (5 * 5) + (5 * 10) + (5 * 15) + ($units - 15) * $unit_price;

        } else if ($units >= 21 AND $units <= 25) {
            $service_charge = 100;
            $unit_price = 58;
            $unit_total = (5 * 5) + (5 * 10) + (5 * 15) + (5 * 40) + ($units - 20) * $unit_price;

        } else if ($units >= 26 AND $units <= 30) {
            $service_charge = 200;
            $unit_price = 88;
            $unit_total = (5 * 5) + (5 * 10) + (5 * 15) + (5 * 40) + (5 * 58) + ($units - 25) * $unit_price;

        } else if ($units >= 31 AND $units <= 40) {
            $service_charge = 400;
            $unit_price = 105;
            $unit_total = (5 * 5) + (5 * 10) + (5 * 15) + (5 * 40) + (5 * 58) + (5 * 88) + ($units - 30) * $unit_price;

        } else if ($units >= 41 AND $units <= 50) {
            $service_charge = 650;
            $unit_price = 120;
            $unit_total = (5 * 5) + (5 * 10) + (5 * 15) + (5 * 40) + (5 * 58) + (5 * 88) + (10 * 105) + ($units - 40) * $unit_price;

        } else if ($units >= 51 AND $units <= 75) {
            $service_charge = 1000;
            $unit_price = 130;
            $unit_total = (5 * 5) + (5 * 10) + (5 * 15) + (5 * 40) + (5 * 58) + (5 * 88) + (10 * 105) + (10 * 120) + ($units - 50) * $unit_price;

        }
        if ($units >= 76) {
            $service_charge = 1600;
            $unit_price = 140;
            $unit_total = (5 * 5) + (5 * 10) + (5 * 15) + (5 * 40) + (5 * 58) + (5 * 88) + (10 * 105) + (10 * 120) + (25 * 130) + ($units - 75) * $unit_price;

        }
        $total = $unit_total + $service_charge;
        return response()->json($total);
    }
}
