<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\Charge;
use Stripe\Stripe;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function index(Request $request)
    {
        $amount = $request->amount;
        $account_no = $request->account_no;
        $invoice_no = $request->invoice_no;
        $month = $request->month;
        $bill_id = $request->bill_id;
        return view('payment.payment')
            ->with('amount', $amount)
            ->with('account_no', $account_no)
            ->with('invoice_no', $invoice_no)
            ->with('month', $month)
            ->with('bill_id', $bill_id);
    }

    public function savePayment(Request $request)
    {
//        dd($request->all());
        Stripe::setApiKey("sk_test_RRI4aCnBgbrNyGh7bqSMzCmj00YWd3D5Dr");

// Token is created using Checkout or Elements!
// Get the payment token ID submitted by the form:
        $token = $_POST['stripeToken'];

        $description = "Invoice - " . $request->invoice_no . " Waterbill payment for " . ucfirst($request->month);

        $charge = Charge::create([
            'amount' => floatval($request->amount) * 100,
            'currency' => 'LKR',
            'description' => $description,
            'source' => $token,
            'metadata' => ['order_id' => $request->invoice_no],
        ]);
        if ($charge->status == "succeeded") {

            $user = User::where('account_no', $request->account_no)->first();

            $payment = new Payment();
            $payment->customer_id = $user->id;
            $payment->bill_id = $request->bill_id;
            $payment->account_no = $request->account_no;
            $payment->payment_method = $charge->source->brand;
            $payment->invoice_no = $request->invoice_no;
            $payment->card_last_digit = $charge->source->last4;
            $payment->total_amount = $request->amount;
            $payment->card_name = $request->card_name;
            $res = $payment->save();

//            balance
            $bill = Bill::where('id', $request->bill_id)->first();
            $balance = floatval($bill->total_amount) - floatval($request->amount);
            $bill->balance = $balance;
            $bill->is_settle = 1;
            $bill->is_settle = 1;
            $bill_res = $bill->save();

            $b = Bill::where('account_no', $request->account_no)->where('id', '!=', $request->bill_id)->update(array('is_settle' => 1));

            $user->due_previous_month = $balance;
            $user_res = $user->save();

//            send sms
            if (!is_null(Auth::user()->contact)) {

                $apiKey = urlencode('cIGgjndT+iE-K6ESkeKNrXndTQ3XJPFH5GxA4uQPtP');

                $numbers = array(Auth::user()->contact);
                $sender = urlencode('water bill');
                $message = rawurlencode($request->invoice_no . " Water bill - " . ucfirst($request->month) . ' of LKR. ' . $request->amount . ' paid. Due amount LKR. ' . $balance);

                $numbers = implode(',', $numbers);

                $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

                $result = $this->post('https://api.txtlocal.com/send/', array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message));
//            dd($result);

            }
            return redirect()->route('lastbill')->with('success', 'Payment Success!');


        } else {
            return redirect()->route('lastbill')->with('error', 'Payment Not Success! Please try again.');
        }
    }

    private function post($url, $postVars = array())
    {
        $postStr = http_build_query($postVars);
        $options = array(
            'http' =>
                array(
                    'method' => 'POST', //We are using the POST HTTP method.
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $postStr //Our URL-encoded query string.
                )
        );
        $streamContext = stream_context_create($options);
        $result = file_get_contents($url, false, $streamContext);
        if ($result === false) {
            $error = error_get_last();
            throw new Exception('POST request failed: ' . $error['message']);
        }
        return $result;
    }
}
