<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ManagerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   public function listBillOfficers(){
       $bill_officers = User::where('role','bill-officer')->get();
       return view('bill.list-bill-officers')
           ->with('bill_officers',$bill_officers);
   }

   public function deleteBillOfficers($id){
       $res = User::where('id',$id)->delete();
       return redirect()->back()->with('success', 'Deleted Success!');
   }
    public function deactiveBillOfficers($id){
        $res = User::where('id',$id)->update(array('status' => 0));
        return redirect()->back()->with('success', 'Deactivated Success!');
    }

    public function activeBillOfficers($id){
        $res = User::where('id',$id)->update(array('status' => 1));
        return redirect()->back()->with('success', 'Activated Success!');
    }

    public function addBillOfficers(){
        return view('bill.add-bill-officer');
    }

    public function saveBillOfficers(Request $request){
        $user = new User($request->except('_token'));
        $user->password = bcrypt($request->password);
        $user->is_complete = 1;
        $user->role = 'bill-officer';
         $res = $user->save();
        return redirect()->back()->with('success', 'Bill Officer added Success!');
    }

//    ===============customer support============
    public function listCustomerSupports(){
        $bill_officers = User::where('role','customer-support')->get();
        return view('customer-support.list-customer-support')
            ->with('bill_officers',$bill_officers);
    }

    public function addCustomerSupports(){
        return view('customer-support.add-customer-support');
    }

    public function saveCustomerSupports(Request $request){
        $user = new User($request->except('_token'));
        $user->password = bcrypt($request->password);
        $user->is_complete = 1;
        $user->role = 'customer-support';
        $res = $user->save();
        return redirect()->back()->with('success', 'Customer Support Officer added Success!');
    }

}


