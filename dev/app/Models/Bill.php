<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
   protected $fillable = ['account_no','invoice_no','meter_no','category','address','month','current_reading','previous_reading','consumption','charges_this_month','due_previous_month','total_amount','balance','is_read','is_settle'];
}
