@extends('layouts.app')

@section('content')

    <div class="wrapper">
        <div class="page-header" style="background-image: url('{{asset('/img/login-image.jpg')}}');">
            <div class="filter"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 ml-auto ">
                        <div class="card card-register">
                            <h3 class="title">Login</h3>

                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">E-Mail </label>
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                </div>
                                <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Password</label>
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                </div>

                                <button class="btn btn-danger btn-block btn-round">Login</button>
                            </form>
                            <div class="forgot">
                                {{--<a href="{{ route('password.request') }}" class="btn btn-link btn-danger">Forgot password?</a>--}}
                                <a href="{{ route('register') }}" class="btn btn-link btn-danger">Create a Account</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
