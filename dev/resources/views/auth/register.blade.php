@extends('layouts.app')

@section('content')

    <div class="wrapper">
        <div class="page-header" style="background-image: url('{{asset('/img/login-image.jpg')}}');">
            <div class="filter"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 ml-auto ">
                        <div class="card card-register">
                            <h3 class="title">Register</h3>

                            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="{{ $errors->has('account_no') ? ' has-error' : '' }}">
                                            <label>Water Bill Account Number</label>
                                            <input id="name" type="text" class="form-control" name="account_no" required
                                                   autofocus>
                                            @if ($errors->has('account_no'))
                                         <span class="help-block">
                                        <strong>{{ $errors->first('account_no') }}</strong>
                                         </span>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <label>Name</label>
                                        <input type="text" class="form-control" placeholder="Last Name" name="name">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <label>NIC </label>
                                        <input type="text" class="form-control" placeholder="NIC Number" name="nic">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Mobile Number</label>
                                        <input type="text" class="form-control" placeholder="947xxxxxxxx"
                                               name="mobile">
                                    </div>
                                </div>
                                <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Email</label>
                                    <input type="email" class="form-control" placeholder="Email" name="email">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <label>Address</label>
                                <input type="text" class="form-control" placeholder="Address" name="address">
                                <label>District</label>
                                <input type="text" class="form-control" placeholder="District" name="district">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Password" name="password">

                                <button type="submit" class="btn btn-danger btn-block btn-round">Register</button>
                            </form>
                            {{--<br>--}}
                            <a href="{{route('login')}}" class="btn btn-link btn-danger">Back to Login</a>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-8 col-md-offset-2">--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">Register</div>--}}

                    {{--<div class="panel-body">--}}
                        {{--<form class="form-horizontal" method="POST" action="{{ route('register') }}">--}}
                            {{--{{ csrf_field() }}--}}

                            {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                                {{--<label for="name" class="col-md-4 control-label">Name</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="name" type="text" class="form-control" name="name"--}}
                                           {{--value="{{ old('name') }}" required autofocus>--}}

                                    {{--@if ($errors->has('name'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                                {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<input id="email" type="email" class="form-control" name="email"--}}
                                           {{--value="{{ old('email') }}" required>--}}
                                    {{--@if ($errors->has('email'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                                {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                    {{--@if ($errors->has('password'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="password-confirm" type="password" class="form-control"--}}
                                           {{--name="password_confirmation" required>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-6 col-md-offset-4">--}}
                                    {{--<button type="submit" class="btn btn-primary">--}}
                                        {{--Register--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection
