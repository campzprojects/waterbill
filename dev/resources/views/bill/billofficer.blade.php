@extends('layouts.admin_layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 ">
                    <div class="box box-success">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="fa fa-user-circle"></i>
                            <h3 class="box-title">Search Customer Here</h3>

                        </div>
                        <div class="box-body">
                            <div class="col-md-12">
                                <table id="example" class="display" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th width="10px">#</th>
                                        <th>Name</th>
                                        <th>NIC</th>
                                        <th>Account No</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $x=1; @endphp
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$x}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->nic}}</td>
                                            <td>{{$user->account_no}}</td>
                                        </tr>
                                        @php $x++; @endphp

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <div class="box box-success">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="fa fa-user-circle"></i>
                            <h3 class="box-title">Select Customer Account</h3>

                        </div>
                        <div class="box-body">
                            <form action="" id="customer_data">
                                {{csrf_field()}}
                                <div class="input-group">
                                    <input class="form-control" placeholder="Enter Customer Account Number"
                                           id="account_no"
                                           name="account_no">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-success" id="select_customer_btn"><i
                                                    class="fa fa-plus"></i> Select Customer
                                        </button>
                                    </div>
                                </div>
                                <p class="error"></p>

                            </form>
                        </div>
                        <!-- /.chat -->
                        {{--<div class="box-footer">--}}

                        {{--</div>--}}
                    </div>

                    <div class="box box-success" id="user_details" style="display: none;">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="fa fa-user-circle"></i>
                            <h3 class="box-title">Enter Billing Information</h3>

                        </div>
                        <div class="box-body">
                            <form action="{{route('billsave')}}" id="bill_data" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="customer_id" id="customer_id">
                                <input type="hidden" name="account_no" id="account_no_1">
                                <div class="form-group">
                                    <label for="">Invoice No</label>
                                    <input class="form-control" placeholder="" id="invoice_no"
                                           name="invoice_no" required readonly>
                                </div>
                                <div class="form-group">
                                    <label for="">Meter No</label>
                                    <input class="form-control" placeholder="" id="meter_no"
                                           name="meter_no" required readonly>
                                </div>
                                <div class="form-group">
                                    <label for="">Category</label>
                                    <select class="form-control" id="category"
                                            name="category" required>
                                        <option value="domestic" selected>Domestic</option>
                                        <option value="industrial">Industrial</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Address</label>
                                    <input class="form-control" placeholder="" id="address"
                                           name="address" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Month</label>
                                    <select class="form-control" id="month"
                                            name="month" required>
                                        <option value="">Select Month</option>
                                        <option value="january" {{(\Carbon\Carbon::now()->format('m') == 1)? 'selected':''}}>
                                            January
                                        </option>
                                        <option value="february" {{(\Carbon\Carbon::now()->format('m') == 2)? 'selected':''}}>
                                            February
                                        </option>
                                        <option value="march" {{(\Carbon\Carbon::now()->format('m') == 3)? 'selected':''}}>
                                            March
                                        </option>
                                        <option value="april" {{(\Carbon\Carbon::now()->format('m') == 4)? 'selected':''}}>
                                            April
                                        </option>
                                        <option value="may" {{(\Carbon\Carbon::now()->format('m') == 5)? 'selected':''}}>
                                            May
                                        </option>
                                        <option value="june" {{(\Carbon\Carbon::now()->format('m') == 6)? 'selected':''}}>
                                            June
                                        </option>
                                        <option value="july" {{(\Carbon\Carbon::now()->format('m') == 7)? 'selected':''}}>
                                            July
                                        </option>
                                        <option value="august" {{(\Carbon\Carbon::now()->format('m') == 8)? 'selected':''}}>
                                            August
                                        </option>
                                        <option value="september" {{(\Carbon\Carbon::now()->format('m') == 9)? 'selected':''}}>
                                            September
                                        </option>
                                        <option value="october" {{(\Carbon\Carbon::now()->format('m') == 10)? 'selected':''}}>
                                            October
                                        </option>
                                        <option value="november" {{(\Carbon\Carbon::now()->format('m') == 11)? 'selected':''}}>
                                            November
                                        </option>
                                        <option value="december" {{(\Carbon\Carbon::now()->format('m') == 12)? 'selected':''}}>
                                            December
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Previous Meter Reading</label>
                                    <input type="number" min="0" class="form-control" placeholder=""
                                           id="previous_reading"
                                           name="previous_reading" required readonly="readonly">
                                </div>

                                <div class="form-group">
                                    <label for="">Current Month Reading</label>
                                    <input type="number" min="0" class="form-control" placeholder=""
                                           id="current_reading"
                                           name="current_reading" required>
                                </div>
                                <div class="form-group">
                                    <label for="">This Month Consumption</label>
                                    <input type="number" min="0" class="form-control" placeholder=""
                                           id="consumption"
                                           name="consumption" required readonly>
                                </div>
                                <div class="form-group">
                                    <label for="">Charges for this month</label>
                                    <input type="number" min="0" class="form-control" placeholder=""
                                           id="charges_this_month"
                                           name="charges_this_month" required readonly="readonly">
                                </div>
                                {{--<div class="form-group">--}}
                                {{--<label for="">Charges for this month</label>--}}
                                {{--<input type="number" min="0" class="form-control" placeholder=""--}}
                                {{--id="charges_this_month"--}}
                                {{--name="charges_this_month" required>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label for="">Due amount</label>
                                    <input type="number" min="0" class="form-control" placeholder=""
                                           id="due_previous_month"
                                           name="due_previous_month" required value="0" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="">Total amount</label>
                                    <input type="number" min="0" class="form-control" placeholder="" id="total_amount"
                                           name="total_amount" value="0" readonly>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success pull-right" id="save_bill"> Save Bill</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.chat -->
                        <div class="box-footer">

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@endsection

@section('extra-css')
    <style>
        .error {
            margin: 5px;
            color: #db3d3d;
        }
    </style>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
        $('#select_customer_btn').click(function (e) {
            e.preventDefault();
            var account_no = $('#account_no').val();
            if (account_no == '') {
                alert('fill account number ');
            } else {
                $.ajax({
                    type: "post",
                    url: '/customer/get/data',
                    data: $('#customer_data').serialize(),
                    beforesend: function (e) {
                        $.blockUI({message: '<h1>Please wait...</h1>'});
                    },
                    success: function (res) {
                        console.log(res.user);
                        if (res.user != null) {
                            $('#invoice_no').val(res.invoice_no);
                            $('#meter_no').val(res.user.meter_no);
                            $('#category').val(res.user.category);
                            $('#address').val(res.user.address);
                            $('#customer_id').val(res.user.id);
                            $('#previous_reading').val((res.last_bill.current_reading == null) ? 0 : res.last_bill.current_reading);
//                            $('#current_reading').val((res.last_bill.current_reading == null) ? 0 : res.last_bill.current_reading);
                            $('#account_no_1').val(res.user.account_no);
                            $('#due_previous_month').val((res.user.due_previous_month == null || res.user.due_previous_month == 0) ? 0 : res.user.due_previous_month);
                            $('#user_details').show(500);

                        } else {
                            $('#user_details').hide(500);
                            alert("Customer account can not find");
                        }

                    }
                });
            }

        });

        $('#charges_this_month').on({
            blur: function (e) {
                $('#total_amount').val(calcBill());
            },
            keyup: function (e) {
                $('#total_amount').val(calcBill());
            }
        });

        $('#current_reading').on({
            blur: function (e) {
                printCons();
            },
//            keyup: function (e) {
//                if ($('#previous_reading').val().length <= $('#current_reading').val().length) {
//                    printCons();
//                }
//            }
        });

        $('#save_bill').click(function () {
            if ($('#current_reading').val() == 0 || $('#current_reading').val() == ''){
                alert('Fill Current Meter Reading');
            }else if ($('#previous_reading').val().length <= $('#current_reading').val().length) {
                $('#bill_data').submit();
            }else{
                alert('Error meter reading');
            }
        });

        function calcBill() {
            var due_previous_month = parseFloat($('#due_previous_month').val());
            var charges_this_month = parseFloat($('#charges_this_month').val());

            var final = due_previous_month + charges_this_month;

            return final;
        }

        function calcConsumption() {
            var prev = parseInt($('#previous_reading').val());
            var current = parseInt($('#current_reading').val());
            if (prev > current) {
                alert('Enter Valid Meter reading')
                 return 0;
            }else{
                return current - prev;
            }

        }

        function printCons() {
            var cons =calcConsumption();
            if (cons>0){
                $('#consumption').val(cons);
                $.ajax({
                    type: "post",
                    url: '/bill-officer/calc/',
                    data: {'units': calcConsumption()},
                    beforesend: function (e) {
                        $.blockUI({message: '<h1>Please wait...</h1>'});
                    },
                    success: function (res) {
                        $('#charges_this_month').val(res);
                        $('#total_amount').val(calcBill());
                    }
                });
            }

        }
    </script>
@endsection
