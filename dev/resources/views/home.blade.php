@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <div class="page-header page-header-xs" data-parallax="true" style="background-image: url('../assets/img/fabio-mangione.jpg');">
            <div class="filter"></div>
        </div>
        <div class="section profile-content">
            <div class="container">

                <div class="owner">
                    <div class="avatar">
                        <img src="../assets/img/faces/joe-gardner-2.jpg" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                    </div>
                    <div class="name">
                        <h4 class="title">{{ Auth::user()->name }} </h4>
                        <h6 class="description">Account Number : {{ Auth::user()->account_no }}</h6>
                        <a href=""> <btn class="btn btn-outline-default btn-round"><i class="fa fa-cog"></i> Edit Details</btn></a>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                         <btn class="btn btn-outline-default btn-round"><i class="fa fa-cog"></i> Logout</btn>
                        </a>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 ml-auto mr-auto text-center">


                    </div>
                </div>
                <br/>
                <footer id="menux">
                    <ul class="menu-wrapper">
                        <li class="active">Pay Your Bill</li>
                        <li>Payment History</li>
                        <li>Check Bills</li>
                        <li>Contact Agent</li>
                    </ul>
                </footer>



            </div>
        </div>
    </div>

@endsection
