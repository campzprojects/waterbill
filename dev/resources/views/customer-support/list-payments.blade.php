@extends('layouts.admin_layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Payments
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Payments</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {{--<div class="container">--}}
        <div class="row">
            <div class="col-md-12 ">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="box box-success">
                    <div class="box-body">
                        <div class="col-md-12">
                            <br>
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th width="10px">#</th>
                                    <th>Account No</th>
                                    <th>Payment Method</th>
                                    <th>Invoice No</th>
                                    <th>Card Name</th>
                                    <th>Card number</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($payments as $p)
                                    <tr>
                                        <td>{{$x}}</td>
                                        <td>{{$p->account_no}}</td>
                                        <td>{{$p->payment_method}}</td>
                                        <td>{{$p->invoice_no}}</td>
                                        <td>{{$p->card_name}}</td>
                                        <td>xxxx xxxx xxxx {{$p->card_last_digit}}</td>
                                        <td>LKR {{$p->total_amount}}</td>

                                    </tr>
                                    @php $x++; @endphp

                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!-- /.chat -->
                    {{--<div class="box-footer">--}}

                    {{--</div>--}}
                </div>

            </div>
        </div>
        {{--</div>--}}
    </section>
    <!-- /.content -->

@endsection

@section('extra-css')
    <style>
        .error {
            margin: 5px;
            color: #db3d3d;
        }
    </style>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });

        function delete_officer(id) {
            var x = confirm('Are you sure to delete?');
            if(x){
                window.location = '/manager/bill-officer/delete/'+id;
            }
        }

        function deactive(id) {
            var x = confirm('Are you sure to Deactivate?');
            if(x){
                window.location = '/manager/bill-officer/deactive/'+id;
            }
        }

        function activate(id) {
            var x = confirm('Are you sure to Activate?');
            if(x){
                window.location = '/manager/bill-officer/active/'+id;
            }
        }
    </script>
@endsection
