@extends('layouts.admin_layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Customer's Bill
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Customer's Bill</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {{--<div class="container">--}}
        <div class="row">
            <div class="col-md-12 ">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="box box-success">
                    <div class="box-body">
                        <div class="col-md-12">
                            <br>
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th width="10px">#</th>
                                    <th>Account No</th>
                                    <th>Invoice No</th>
                                    <th>Meter No</th>
                                    <th>Category</th>
                                    <th>Address</th>
                                    <th>Month</th>
                                    <th width="250px">Charge</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($bills as $bill)
                                    <tr>
                                        <td>{{$x}}</td>
                                        <td>{{$bill->account_no}}</td>
                                        <td>{{$bill->invoice_no}}</td>
                                        <td>{{$bill->meter_no}}</td>
                                        <td>{{$bill->category}}</td>
                                        <td>{{$bill->address}}</td>
                                        <td>{{$bill->month}}</td>
                                        <td>Charge this Month :
                                            LKR {{($bill->charges_this_month)?$bill->charges_this_month:'0.00'}}<br>
                                            Due Previous Month :
                                            LKR {{($bill->due_previous_month)?$bill->due_previous_month:'0.00'}}<br>
                                            Total : LKR {{($bill->total_amount)?$bill->total_amount:'0.00'}}<br>
                                            Balance : LKR {{($bill->balance)?$bill->balance:'0.00'}}</td>
                                        <td>
                                            {!! ($bill->is_settle)? '<span class="badge badge-success">Settled Payment</span>':'<span class="badge badge-danger">Not Settled</span>'!!}
                                        </td>
                                        <td>
                                            @if(!$bill->is_settle)
                                                @if(is_null($bill->red_notice))
                                                    &nbsp;&nbsp;&nbsp;<input type="date" id="red_date_{{$bill->id}}"
                                                                             min="{{\Carbon\Carbon::now()->format('Y-m-d')}}">
                                                    <button class="btn btn-danger" onclick="rednotice({{$bill->id}});">
                                                        Send
                                                        Red Notice
                                                    </button>
                                                @elseif(\Carbon\Carbon::parse($bill->red_notice)->format('Y-m-d'))
                                                    <p class="text-danger">Red Notice sent on
                                                        <b>{{\Carbon\Carbon::parse($bill->red_notice)->format('Y-m-d')}}</b>.
                                                        Red notice due date is
                                                        <b>{{\Carbon\Carbon::parse($bill->red_notice_due)->format('Y-m-d')}}</b>
                                                    </p>
                                                    <button class="btn btn-info"
                                                            onclick="removerednotice({{$bill->id}})">Remove Red Notice
                                                    </button>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                    @php $x++; @endphp

                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!-- /.chat -->
                    {{--<div class="box-footer">--}}

                    {{--</div>--}}
                </div>

            </div>
        </div>
        {{--</div>--}}
    </section>
    <!-- /.content -->

@endsection

@section('extra-css')
    <style>
        .error {
            margin: 5px;
            color: #db3d3d;
        }
    </style>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });

        function delete_officer(id) {
            var x = confirm('Are you sure to delete?');
            if (x) {
                window.location = '/manager/bill-officer/delete/' + id;
            }
        }

        function deactive(id) {
            var x = confirm('Are you sure to Deactivate?');
            if (x) {
                window.location = '/manager/bill-officer/deactive/' + id;
            }
        }

        function activate(id) {
            var x = confirm('Are you sure to Activate?');
            if (x) {
                window.location = '/manager/bill-officer/active/' + id;
            }
        }
        function rednotice(id) {
            var x = confirm('Are you sure to send a red notice to this customer?');
            var date = document.getElementById('red_date_' + id).value;
            if (date == '') {
                alert('Please fill date');
            } else {
                if (x) {
                    window.location = '/customer-support/customers/bills/red-notice/' + id + '/' + date;
                }
            }

        }
        function removerednotice(id) {
            var x = confirm('Are you sure to remove the red notice to this customer?');
            if (x) {
                window.location = '/customer-support/customers/bills/red-notice-remove/' + id ;
            }
        }
    </script>
@endsection
