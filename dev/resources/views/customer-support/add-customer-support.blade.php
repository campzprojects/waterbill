@extends('layouts.admin_layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Customer Support Officer
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{asset('')}}home"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Customer Support Officer</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 ">
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <div class="box box-success">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="fa fa-user-circle"></i>
                            <h3 class="box-title">Add Bill Officer</h3>

                        </div>
                        <div class="box-body">
                            <form action="{{route('saveCustomerSupport')}}" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="">Name</label>
                                    <input class="form-control" placeholder="name" id=""
                                           name="name" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" class="form-control" placeholder="Email" id=""
                                           name="email" required>
                                </div>
                                <div class="form-group">
                                    <label for="">NIC Number</label>
                                    <input class="form-control" placeholder="NIC" id=""
                                           name="nic" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Contact</label>
                                    <input class="form-control" placeholder="Contact" id=""
                                           name="contact" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Role</label>
                                    <input class="form-control" placeholder="Role" id=""
                                           name="role" required readonly value="customer-support">
                                </div>
                                <div class="form-group">
                                    <label for="">Password</label>
                                    <input type="password" class="form-control" placeholder="Password" id=""
                                           name="password" required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success pull-right">Add</button>
                                </div>
                                <p class="error"></p>

                            </form>
                        </div>
                        <!-- /.chat -->
                        {{--<div class="box-footer">--}}

                        {{--</div>--}}
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@endsection

@section('extra-css')
    <style>
        .error {
            margin: 5px;
            color: #db3d3d;
        }
    </style>
@endsection

@section('extra-js')

@endsection
