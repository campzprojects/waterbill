@extends('layouts.admin_layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Customers
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Customers</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {{--<div class="container">--}}
        <div class="row">
            <div class="col-md-12 ">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="box box-success">
                    <div class="box-body">
                        <div class="col-md-12">
                            <br>
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th width="10px">#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>NIC</th>
                                    <th>Account No</th>
                                    <th>Address</th>
                                    <th>District</th>
                                    <th>Contact</th>
                                    <th>Meter No</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$x}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->nic}}</td>
                                        <td>{{$user->account_no}}</td>
                                        <td>{{$user->address}}</td>
                                        <td>{{$user->district}}</td>
                                        <td>{{$user->contact}}</td>
                                        <td>{{$user->meter_no}}</td>
                                        <td>
                                            {!! ($user->status)? '<span class="badge badge-success">Active</span>':'<span class="badge badge-danger">Deactive</span>'!!}
                                        </td>
                                        <td>
                                            @if($user->status)
                                                <button class="btn btn-warning" onclick="deactive({{$user->id}});">Deactivate</button>
                                            @else
                                                <button class="btn btn-success" onclick="activate({{$user->id}});">Activate</button>
                                            @endif
                                            &nbsp;&nbsp;&nbsp;<button class="btn btn-danger" onclick="delete_officer({{$user->id}});">Delete</button>
                                        </td>
                                    </tr>
                                    @php $x++; @endphp

                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!-- /.chat -->
                    {{--<div class="box-footer">--}}

                    {{--</div>--}}
                </div>

            </div>
        </div>
        {{--</div>--}}
    </section>
    <!-- /.content -->

@endsection

@section('extra-css')
    <style>
        .error {
            margin: 5px;
            color: #db3d3d;
        }
    </style>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });

        function delete_officer(id) {
            var x = confirm('Are you sure to delete?');
            if(x){
                window.location = '/manager/bill-officer/delete/'+id;
            }
        }

        function deactive(id) {
            var x = confirm('Are you sure to Deactivate?');
            if(x){
                window.location = '/manager/bill-officer/deactive/'+id;
            }
        }

        function activate(id) {
            var x = confirm('Are you sure to Activate?');
            if(x){
                window.location = '/manager/bill-officer/active/'+id;
            }
        }
    </script>
@endsection
