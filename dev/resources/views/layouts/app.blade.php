<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>Water Biling System</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>

    <!-- Bootstrap core CSS     -->
    <link href="{{asset('/css/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('/css/css/paper-kit.css?v=2.1.0')}}" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{asset('/css/css/nucleo-icons.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    @yield('extra-css')

    @php
        $rednotice=false;
        if (\Illuminate\Support\Facades\Auth::user()){
        if (\Illuminate\Support\Facades\Auth::user()->role !='customer'){ @endphp
    <script>
        window.location = '/home';
    </script>
        @php }
         $bill= \App\Models\Bill::where('account_no',\Illuminate\Support\Facades\Auth::user()->account_no)->orderBy('created_at','DESC')->first();
        $url = array_slice(explode('/', request()->route()->uri()), -1, 1);
            if ($bill){
                if (\Carbon\Carbon::parse($bill->red_notice)->format('Y-m-d') < \Carbon\Carbon::now()->format('Y-m-d') AND $url[0] != 'last-bill'){
                    $rednotice=true;
                }
            }
        }

    @endphp
</head>
<body>
<nav class="navbar navbar-expand-md fixed-top navbar-transparent">
    <div class="container">
        <div class="navbar-translate">
            <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse"
                    data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
            </button>
            <a class="navbar-brand" href="">Water Billing</a>
            @guest
            {{--<li><a href="{{ route('login') }}">Login</a></li>--}}
            {{--<li><a href="">Register</a></li>--}}
            @else
                {{--<li class="dropdown">--}}
                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>--}}
                {{--<span class="caret"></span>--}}
                {{--</a>--}}


                {{--</li>--}}
                @endguest
        </div>

    </div>
</nav>

@yield('content')
<footer class="footer section-dark">
    <div class="container">
        <div class="row">
            <nav class="footer-nav">
                {{--<ul>--}}
                    {{--<li><a href="https://www.creative-tim.com">Contact Us</a></li>--}}
                    {{--<li><a href="http://blog.creative-tim.com">News</a></li>--}}
                    {{--<li><a href="https://www.creative-tim.com/license">Faq</a></li>--}}
                {{--</ul>--}}
            </nav>
            <div class="credits ml-auto">
                    <span class="copyright">
                        Water Bill System &copy; 2019 All Rights Reserved
                    </span>
            </div>
        </div>
    </div>
</footer>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
</body>

<!-- Core JS Files -->
<script src="{{asset('/js/js/jquery-3.2.1.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/js/jquery-ui-1.12.1.custom.min.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>

<script src="{{asset('/js/js/bootstrap.min.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('/js/js/tether.min.js')}}" type="text/javascript"></script>--}}

<!--  Paper Kit Initialization snd functons -->
{{--<script src="{{asset('')}}assets/js/paper-kit.js?v=2.0.1"
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>></script>--}}
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
@yield('extra-js')

@if($rednotice)
    <script>
        $.confirm({
            icon: 'fa fa-exclamation-triangle',
            title: 'Warning',
            content: 'Please settle bill immediately to keep water connection.',
            theme: 'modern',
            closeIcon: true,
            animation: 'scale',
            type: 'red',
            buttons: {
                okay: {
                    text: 'Pay Bill',
                    btnClass: 'btn-red',
                    action: function () {
                        window.location = '/profile/last-bill'
                    }
                }
            }
        });
    </script>
@endif
</html>
