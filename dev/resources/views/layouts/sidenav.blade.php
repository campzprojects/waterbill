<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('')}}image/{{$user->role}}.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ucfirst($user->name)}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> {{ucfirst(str_replace('-',' ',$user->role))}}</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            @if($user->role == 'bill-officer')
                <li class=" treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Bills</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{asset('')}}bill/bill-officer/mark"><i class="fa fa-circle-o"></i>
                                Mark a Bill</a></li>
                        {{--<li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>--}}
                    </ul>
                </li>
            @endif
            @if($user->role == 'customer-support')
                <li>
                    <a href="{{asset('')}}customer-support/customers/list">
                        <i class="fa fa-dashboard"></i> <span>Customers List</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                </li>
                <li class=" treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Customer's Bills</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{asset('')}}customer-support/customers/bills/all"><i class="fa fa-circle-o"></i>
                                All Bills</a></li>
                        <li class="active"><a href="{{asset('')}}customer-support/customers/bills/settled"><i class="fa fa-circle-o"></i>
                                Settled Bills</a></li>
                        <li class="active"><a href="{{asset('')}}customer-support/customers/bills/not-settled"><i class="fa fa-circle-o"></i>
                                Not Settled Bills</a></li>
                        {{--<li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>--}}
                    </ul>
                </li>
                <li>
                    <a href="{{asset('')}}customer-support/payments/list">
                        <i class="fa fa-dashboard"></i> <span>Payments</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                </li>
            @endif
            @if($user->role == 'manager')
                <li class=" treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Bill Officers</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class=""><a href="{{asset('')}}manager/bill-officer/add"><i class="fa fa-circle-o"></i>
                                Add Bill Officers</a></li>
                        <li class=""><a href="{{asset('')}}manager/bill-officer/list"><i class="fa fa-circle-o"></i>
                                List Bill Officers</a></li>
                        {{--<li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>--}}
                    </ul>
                </li>
                <li class=" treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Customer Support Officers</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class=""><a href="{{asset('')}}manager/customer-support/add"><i class="fa fa-circle-o"></i>
                                 Add Customer Support Officers</a></li>
                        <li class=""><a href="{{asset('')}}manager/customer-support/list"><i class="fa fa-circle-o"></i>
                                List Customer Support Officers</a></li>
                        {{--<li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>--}}
                    </ul>
                </li>
            @endif
            {{--<li>--}}
                {{--<a href="pages/widgets.html">--}}
                    {{--<i class="fa fa-th"></i> <span>Widgets</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<small class="label pull-right bg-green">new</small>--}}
            {{--</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            {{--<li class="header">LABELS</li>--}}
            {{--<li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>--}}
            {{--<li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>--}}
            {{--<li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>--}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>