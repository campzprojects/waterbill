@extends('layouts.app')

@section('content')
    @php
        $user = \Illuminate\Support\Facades\Auth::user();
    @endphp
    <div class="wrapper">
        <div class="page-header page-header-xs" data-parallax="true"
             style="background-image: url('../assets/img/fabio-mangione.jpg');">
            <div class="filter"></div>
        </div>
        <div class="section profile-content">
            <div class="container">

                <div class="owner">
                    <div class="avatar">
                        <img src="{{asset('')}}image/{{($user->profile_image)? $user->profile_image :'profile.png' }}"
                             alt="Circle Image" class="img-circle img-no-padding img-responsive">
                    </div>
                    <div class="name">
                        <h4 class="title">{{$user->name}}<br/></h4>
                        <h6 class="description">Account Number : {{ $user->account_no }}</h6>
                        <a href="{{asset('/profile/edit')}}">
                            <btn class="btn btn-outline-default btn-round"><i class="fa fa-cog"></i> Edit Details</btn>
                        </a>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <btn class="btn btn-outline-default btn-round"><i class="fa fa-cog"></i> Logout</btn>
                        </a>
                        <br>
                        <br>
                        <br>
                        <h6 class="description">
                            Total Amount Payable <br><br>
                            <span class="{{($user->due_previous_month>0)?'overdue_span':'owe_span'}}">LKR. {{$user->due_previous_month}}</span>

                        </h6>
                    </div>
                </div>

                <br/>
                <br/>
                <div id="menux">
                    <ul class="menu-wrapper">
                        <li class="active"><a href="{{asset('')}}profile">Profile</a></li>
                        <li class=""><a href="{{asset('')}}profile/last-bill">Last Bill</a></li>
                        <li><a href="{{asset('')}}profile/billing-history">Billing History</a></li>
                        <li class=""><a href="{{asset('')}}profile/payment-history">Payment History</a></li>

                    </ul>
                </div>
                <br>
                {{--edit--}}
                @if(!$user->is_complete)
                    <div class="row">
                        <div class="col-md-8 form-div text-center">
                            <br>
                            <p class="text-danger">Please Update Your Information!</p>
                            <a href="{{asset('/profile/edit')}}">
                                <btn class="btn btn-outline-default btn-round"><i class="fa fa-cog"></i> Edit Details</btn>
                            </a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection

@section('extra-css')
    <style>
        #menux .active a {
            color: #fff !important;
            font-weight: 600;
        }

        #menux a {
            color: #222;
            font-weight: 600;

        }

        .owe_span {
            background: #38bf0d;
            border-radius: 20px;
            padding: 9px;
            color: #fff;
        }

        .overdue_span {
            background: #d80a0a;
            border-radius: 20px;
            padding: 9px;
            color: #fff;
        }

        .form-div {
            float: none;
            margin: 0px auto;
            background: rgba(238, 238, 238, 0.6784313725490196);
            padding-bottom: 15px;
        }
    </style>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
            $('#paynow_btn').click(function (e) {
                $('#payment_details_form').submit();
            });

        });
    </script>
@endsection
