@extends('layouts.app')

@section('content')

    <div class="wrapper">
        <div class="page-header page-header-xs" data-parallax="true"
             style="background-image: url('../assets/img/fabio-mangione.jpg');">
            <div class="filter"></div>
        </div>
        <div class="section profile-content">
            <div class="container">
                <div class="owner">
                    <div class="avatar">
                        <img src="{{asset('')}}image/{{(\Illuminate\Support\Facades\Auth::user()->profile_image)?\Illuminate\Support\Facades\Auth::user()->profile_image :'profile.png' }}"
                             alt="Circle Image" class="img-circle img-no-padding img-responsive">
                    </div>
                    <div class="name">
                        <h4 class="title">{{\Illuminate\Support\Facades\Auth::user()->name}}<br/></h4>
                        <h6 class="description">Account Number : {{ Auth::user()->account_no }}</h6>
                        <a href="{{asset('/profile/edit')}}">
                            <btn class="btn btn-outline-default btn-round"><i class="fa fa-cog"></i> Edit Details</btn>
                        </a>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <btn class="btn btn-outline-default btn-round"><i class="fa fa-cog"></i> Logout</btn>
                        </a>
                        <br>
                        <br>
                        <br>
                        <h6 class="description">
                            Total Amount Payable <br><br>
                            <span class="{{(\Illuminate\Support\Facades\Auth::user()->due_previous_month>0)?'overdue_span':'owe_span'}}">LKR. {{\Illuminate\Support\Facades\Auth::user()->due_previous_month}}</span>
                            <br><br>
                            {{--@if(\Illuminate\Support\Facades\Auth::user()->due_previous_month>0)--}}
                                {{--<button class="btn btn-success btn-lg">Pay Now--}}
                                    {{--LKR. {{\Illuminate\Support\Facades\Auth::user()->due_previous_month}} </button>--}}
                            {{--@endif--}}
                        </h6>
                    </div>
                </div>

                <br/>
                <br/>
                <div id="menux">
                    <ul class="menu-wrapper">
                        <li class=""><a href="{{asset('')}}profile">Profile</a></li>
                        <li><a href="{{asset('')}}profile/last-bill">Last Bill</a></li>
                        <li class="active"><a href="{{asset('')}}profile/billing-history">Billing History</a></li>
                        <li class=""><a href="{{asset('')}}profile/payment-history">Payment History</a></li>
                    </ul>
                </div>

                <br/>
                <br/>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Payment History</h3><br>
                        <table id="example" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th width="10px">#</th>
                                <th>Invoice No</th>
                                <th>Details</th>
                                <th>Month</th>
                                <th>Charges</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $x = 1; @endphp
                            @foreach($payment_histories as $one)
                                @if($x)
                                    <form action="{{route('payment')}}" method="post" id="payment_form">
                                        <input type="hidden" name="amount"
                                               value="{{\Illuminate\Support\Facades\Auth::user()->due_previous_month}}">
                                        <input type="hidden" name="invoice_no" value="{{$one->invoice_no}}">
                                    </form>
                                @endif
                                <tr>
                                    <td>{{$x}}</td>
                                    <td>{{$one->invoice_no}}</td>
                                    <td>Meter No : {{$one->meter_no}}</td>
                                    <td>{{ucfirst($one->month)}}</td>
                                    <td>This Month Charge : <b>LKR {{$one->charges_this_month}}</b><br>
                                        Due Payment : <b>LKR {{$one->due_previous_month}}</b><br>
                                        Total Payable : <b>LKR {{$one->total_amount}}</b><br></td>
                                    <td>
                                        @if($one->is_settle == 0)
                                            @if($one->total_amount > 0)
                                                <button class="btn btn-sm btn-success">
                                                    Pay Now
                                                    <b>LKR. {{$one->total_amount}}</b></button>
                                            @else
                                                <h6 class="description">
                                                    Total Payable <b>LKR. {{$one->total_amount}}</b>
                                                </h6>
                                            @endif
                                        @else
                                            <span class="owe_span"><i class="ion-checkmark"></i>Settled</span>
                                        @endif
                                        <a href="{{asset('')}}invoice/view/{{$one->id}}">
                                            <button class="btn btn-sm btn-info">View Invoice</button>
                                        </a>
                                    </td>
                                </tr>
                                @php $x++; @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('extra-css')
    <style>
        .overdue_span {
            background: #d80a0a;
            border-radius: 20px;
            padding: 9px;
            color: #fff;
        }

        .owe_span {
            background: #38bf0d;
            border-radius: 20px;
            padding: 9px;
            color: #fff;
        }

        #menux .active a {
            color: #fff !important;
            font-weight: 600;
        }

        #menux a {
            color: #222;
            font-weight: 600;

        }
    </style>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endsection
