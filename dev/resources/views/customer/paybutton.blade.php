<form action="{{route('payment_form')}}" method="post" id="payment_details_form">
    {{csrf_field()}}
    <input type="hidden" name="amount" value="{{$amount}}">
    <input type="hidden" name="account_no" value="{{$account_no}}">
    <input type="hidden" name="invoice_no" value="{{$invoice_no}}">
    <input type="hidden" name="month" value="{{$month}}">
    <input type="hidden" name="bill_id" value="{{$bill_id}}">

</form>