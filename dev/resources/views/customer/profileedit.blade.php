@extends('layouts.app')

@section('content')
    <div class="wrapper">
        <div class="page-header page-header-xs" data-parallax="true"
             style="background-image: url('../assets/img/fabio-mangione.jpg');">
            <div class="filter"></div>
        </div>
        <div class="section profile-content">
            <div class="container">

                <div class="owner">
                    <div class="avatar">
                        <img src="../assets/img/faces/joe-gardner-2.jpg" alt="Circle Image"
                             class="img-circle img-no-padding img-responsive">
                    </div>
                    <div class="name">
                        <h4 class="title">{{ Auth::user()->name }} </h4>
                        <h6 class="description">Account Number : {{ Auth::user()->account_no }}</h6>
                        <a href="{{asset('/profile')}}">
                            <btn class="btn btn-outline-default btn-round"><i class="fa fa-home"></i> Home</btn>
                        </a>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <btn class="btn btn-outline-default btn-round"><i class="fa fa-cog"></i> Logout</btn>
                        </a>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 ml-auto mr-auto text-center">


                    </div>
                </div>
                <br/>
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ route('update-profile') }}">
                    {{csrf_field()}}
                    <div class="form-group col-md-6">
                        <input type="hidden" class="form-control" placeholder="name" value="{{ Auth::user()->id }}"
                               name="id">
                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-6">
                            <label for="">Name</label>
                            <input type="text" class="form-control" placeholder="name" value="{{ Auth::user()->name }}"
                                   name="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">E mail</label>
                            <input type="email" class="form-control" id="email" placeholder="email"
                                   value="{{ Auth::user()->email }}" name="email">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="">Account Number</label>
                            <input type="text" class="form-control" id="account_no" placeholder=""
                                   value="{{ Auth::user()->account_no }}" name="account_no">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">Meter Number</label>
                            <input type="text" class="form-control" id="meter_no" placeholder="Meter Number"
                                   value="{{ Auth::user()->meter_no }}" name="meter_no">
                        </div>
                    </div>
                    <div class="form-row ">
                        <div class="form-group col-md-6">
                            <label for="inputAddress">Company</label>
                            <input type="text" class="form-control" id="inputAddress" placeholder="Company or Home"
                                   value="{{ Auth::user()->company }}" name="company">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">Nic</label>
                            <input type="text" class="form-control" id="inputAddress" placeholder="Nic Number"
                                   value="{{ Auth::user()->nic }}" name="nic">
                        </div>
                    </div>
                    <div class="form-row ">
                        <div class="form-group col-md-6">
                            <label for="inputAddress">Mobile Number</label>
                            <input type="text" class="form-control" id="contact" placeholder="Contact Number"
                                   value="{{ Auth::user()->contact }}" name="contact">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">Address</label>
                            <input type="text" class="form-control" id="address" placeholder="Address"
                                   value="{{ Auth::user()->address }}" name="address">
                        </div>
                    </div>



                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputAddress">District</label>
                            <input type="text" class="form-control" id="inputAddress" placeholder="Company or Home"
                                   value="{{ Auth::user()->company }}" name="company">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputCity">district</label>
                            <input type="text" class="form-control" id="inputCity" name="district">
                        </div>


                    </div>
                    <div class="form-row ">
                        <div class="form-group col-md-6">
                            <label for="inputAddress">Change Password</label>
                            <input type="password" class="form-control" id="contact" placeholder="password"
                                   value="" name="password">
                        </div>
                    </div>


                    <button type="submit" class="btn btn-primary">Update Details</button>
                </form>


            </div>
        </div>
    </div>

@endsection

<f