<html>
<head>
    <title>View Invoice</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <style>
        #invoice {
            padding: 30px;
        }

        .invoice {
            position: relative;
            background-color: #FFF;
            min-height: 680px;
            padding: 15px
        }

        .invoice header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #3989c6
        }

        .invoice .company-details {
            text-align: right
        }

        .invoice .company-details .name {
            margin-top: 0;
            margin-bottom: 0
        }

        .invoice .contacts {
            margin-bottom: 20px
        }

        .invoice .invoice-to {
            text-align: left
        }

        .invoice .invoice-to .to {
            margin-top: 0;
            margin-bottom: 0
        }

        .invoice .invoice-details {
            text-align: right
        }

        .invoice .invoice-details .invoice-id {
            margin-top: 0;
            color: #3989c6
        }

        .invoice main {
            padding-bottom: 50px
        }

        .invoice main .thanks {
            margin-top: -100px;
            font-size: 2em;
            margin-bottom: 50px
        }

        .invoice main .notices {
            padding-left: 6px;
            border-left: 6px solid #3989c6
        }

        .invoice main .notices .notice {
            font-size: 1.2em
        }

        .invoice table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px
        }

        .invoice table td, .invoice table th {
            padding: 15px;
            background: #eee;
            border-bottom: 1px solid #fff
        }

        .invoice table th {
            white-space: nowrap;
            font-weight: 400;
            font-size: 16px
        }

        .invoice table td h3 {
            margin: 0;
            font-weight: 400;
            color: #3989c6;
            font-size: 1.2em
        }

        .invoice table .qty, .invoice table .total, .invoice table .unit {
            text-align: right;
            font-size: 1.2em
        }

        .invoice table .no {
            color: #fff;
            font-size: 1.6em;
            background: #3989c6
        }

        .invoice table .unit {
            background: #ddd
        }

        .invoice table .total {
            background: #3989c6;
            color: #fff
        }

        .invoice table tbody tr:last-child td {
            border: none
        }

        .invoice table tfoot td {
            background: 0 0;
            border-bottom: none;
            white-space: nowrap;
            text-align: right;
            padding: 10px 20px;
            font-size: 1.2em;
            border-top: 1px solid #aaa
        }

        .invoice table tfoot tr:first-child td {
            border-top: none
        }

        .invoice table tfoot tr:last-child td {
            color: #3989c6;
            font-size: 1.4em;
            border-top: 1px solid #3989c6
        }

        .invoice table tfoot tr td:first-child {
            border: none
        }

        .invoice footer {
            width: 100%;
            text-align: center;
            color: #777;
            border-top: 1px solid #aaa;
            padding: 8px 0
        }

        @media print {
            .invoice {
                font-size: 11px !important;
                overflow: hidden !important
            }

            .invoice footer {
                position: absolute;
                bottom: 10px;
                page-break-after: always
            }

            .invoice > div:last-child {
                page-break-before: always
            }
        }
    </style>
</head>
<body>
<div id="invoice" style="position: relative;">

    <div class="toolbar hidden-print">
        <div class="">
            <a href="{{asset('')}}profile/last-bill">
                <button class="btn btn-warning pull-left"><i class="fa fa-arrow-left"></i> Back</button>
            </a>
            <button id="printInvoice" class="btn btn-info pull-right"><i class="fa fa-print"></i> Print Invoice</button>
        </div>
        <div class="clearfix"></div>
        <hr>
    </div>
    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="col">
                        <a target="_blank" href="#">
                            <h2>Water Bill- Month of {{ucfirst($bill_info->month)}}</h2>
                        </a>
                    </div>
                    <div class="col company-details">
                        <h2 class="name">
                            <a target="_blank" href="#">
                                Water Board
                            </a>
                        </h2>
                        <div>2nd Street, Colombo 05</div>
                        <div>011-2248563</div>
                        <div>info@waterboard.lk</div>
                    </div>
                </div>
            </header>
            <main>
                <div class="row contacts">
                    <div class="col invoice-to">
                        <div class="text-gray-light">INVOICE TO:</div>
                        <h2 class="to">{{$user->name}}</h2>
                        <div class="address">{{$user->address}}</div>
                        <div class="email"><a href="{{$user->email}}">{{$user->email}}</a></div>
                        <div class="address">{{$user->contact}}</div>
                    </div>
                    <div class="col invoice-details">
                        <h1 class="invoice-id">{{$bill_info->invoice_no}}</h1>
                        <div class="date">Date of
                            Invoice: {{\Carbon\Carbon::parse($bill_info->created_at)->format('Y-m-d')}}</div>
                        <div class="date">Due
                            Date: {{\Carbon\Carbon::parse($bill_info->created_at)->addDays(30)->format('Y-m-d')}}</div>
                        <div class="date">Month of <b>{{ucfirst($bill_info->month)}}</b></div>
                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th class="text-left">DESCRIPTION</th>
                        <th class="text-right">TOTAL</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="no">01</td>
                        <td class="text-left">
                            <h3>
                                Water Usage bill for month of <b>{{ucfirst($bill_info->month)}}</b>
                            </h3>
                        </td>
                        <td class="total">LKR. {{number_format((float)$bill_info->charges_this_month, 2, '.', '')}}</td>
                    </tr>
                    @if($bill_info->due_previous_month AND $bill_info->due_previous_month>0)
                        <tr>
                            <td class="no">02</td>
                            <td class="text-left">
                                <h3>
                                    Previous month due amount
                                </h3>
                            </td>
                            <td class="total">
                                LKR. {{number_format((float)$bill_info->due_previous_month, 2, '.', '')}}</td>
                        </tr>
                    @endif
                    @for($x=1;$x<7;$x++)
                        <tr>
                            <td class="no"></td>
                            <td class="text-left">
                            </td>
                            <td class="total"></td>
                        </tr>
                    @endfor

                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2">Total</td>
                        <td>LKR. {{number_format((float)$bill_info->total_amount, 2, '.', '')}}</td>
                    </tr>
                    </tfoot>
                </table>
                <br><br><br>
                <div class="notices">
                    <div class="notice">Outdoor water use restrictions - all customers are required to follow the year
                        round
                        conservation rules listed on the back of this bill. Violators are subject to fines in
                        accordance with Water Authority Rules.
                    </div>
                    <br>
                    <div class="text-danger">NOTICE:</div>
                    <div class="notice text-danger">Please make all payments withing 30 days from invoice date.</div>
                </div>
            </main>
            <footer>
                Invoice was created via Water Bill System on {{\Carbon\Carbon::now()->format('Y-m-d H:i:s')}}
            </footer>
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
    @if($bill_info->is_settle)
        <img src="{{asset('')}}image/paid.png" alt="" class="paid">
    @endif
</div>
</body>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $('#printInvoice').click(function () {
        Popup($('.invoice')[0].outerHTML);

        function Popup(data) {
            window.print();
            return true;
        }
    });
</script>
<style>
    .paid {
          width: 200px;
          position: absolute;
          top: 575px;
          left: 42%;
          opacity: 0.6;
      }
</style>
</html>