@extends('layouts.app')

@section('content')
    @php
        $bill= \App\Models\Bill::where('account_no',\Illuminate\Support\Facades\Auth::user()->account_no)->orderBy('created_at','DESC')->first();
        $rednotice=false;
        $url = array_slice(explode('/', request()->route()->uri()), -1, 1);
        if ($bill){
            if (\Carbon\Carbon::parse($bill->red_notice)->format('Y-m-d') < \Carbon\Carbon::now()->format('Y-m-d')){
                $rednotice=true;
            }
        }
    @endphp
    <div class="wrapper">
        <div class="page-header page-header-xs" data-parallax="true"
             style="background-image: url('../assets/img/fabio-mangione.jpg');">
            <div class="filter"></div>
        </div>
        <div class="section profile-content">
            <div class="container">

                <div class="owner">
                    <div class="avatar">
                        <img src="{{asset('')}}image/{{(\Illuminate\Support\Facades\Auth::user()->profile_image)?\Illuminate\Support\Facades\Auth::user()->profile_image :'profile.png' }}"
                             alt="Circle Image" class="img-circle img-no-padding img-responsive">
                    </div>
                    <div class="name">
                        <h4 class="title">{{\Illuminate\Support\Facades\Auth::user()->name}}<br/></h4>
                        <h6 class="description">Account Number : {{ Auth::user()->account_no }}</h6>
                        <a href="{{asset('/profile/edit')}}">
                            <btn class="btn btn-outline-default btn-round"><i class="fa fa-cog"></i> Edit Details</btn>
                        </a>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <btn class="btn btn-outline-default btn-round"><i class="fa fa-cog"></i> Logout</btn>
                        </a>
                        <br>
                        <br>
                        <br>
                        <h6 class="description">
                            Total Amount Payable <br><br>
                            <span class="{{(\Illuminate\Support\Facades\Auth::user()->due_previous_month>0)?'overdue_span':'owe_span'}}">LKR. {{\Illuminate\Support\Facades\Auth::user()->due_previous_month}}</span>

                        </h6>
                    </div>
                </div>

                <br/>
                <br/>
                <div id="menux">
                    <ul class="menu-wrapper">
                        <li class=""><a href="{{asset('')}}profile">Profile</a></li>
                        <li class="active"><a href="{{asset('')}}profile/last-bill">Last Bill</a></li>
                        <li><a href="{{asset('')}}profile/billing-history">Billing History</a></li>
                        <li class=""><a href="{{asset('')}}profile/payment-history">Payment History</a></li>

                    </ul>
                </div>
                {{--                {{dd(\Illuminate\Support\Facades\Auth::user())}}--}}
                <div class="row">
                    <div class="col-md-12 text-center">
                        <br><br>
                        @if($rednotice)
                            <p class="text-center text-danger"><b>Warning!<br>
                                    Please settle bill immediately to keep water connection.</b></p><br>
                        @endif
                        @if(\Illuminate\Support\Facades\Auth::user()->due_previous_month >0)
                            <button class="btn btn-{{($rednotice)?'danger':'success'}} btn-lg" id="paynow_btn">Pay Now
                                LKR. {{number_format((float)\Illuminate\Support\Facades\Auth::user()->due_previous_month, 2, '.', '')}} </button>
                        @endif

                        @if(session()->has('success'))
                            <br><br>
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <br><br>
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                        @endif
                    </div>
                    <hr>
                </div>
                <br>
                <hr>
                @if($bill)
                    <div class="row">
                        <div class="col-md-12">
                            <div id="invoice" style="position: relative;">

                                <div class="toolbar hidden-print">
                                    <div class="text-right">
                                        <a href="{{asset('')}}invoice/view/{{$bill_info->id}}">
                                            <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i>
                                                Print
                                                Invoice
                                            </button>
                                        </a>
                                    </div>
                                    <hr>
                                </div>
                                <div class="invoice overflow-auto">
                                    <div style="min-width: 600px">
                                        <header>
                                            <div class="row">
                                                <div class="col">
                                                    <a target="_blank" href="#">
                                                        <h2>Water Bill- Month of {{ucfirst($bill_info->month)}}</h2>
                                                    </a>
                                                </div>
                                                <div class="col company-details">
                                                    <h2 class="name">
                                                        <a target="_blank" href="#">
                                                            Water Board
                                                        </a>
                                                    </h2>
                                                    <div>2nd Street, Colombo 05</div>
                                                    <div>011-2248563</div>
                                                    <div>info@waterboard.lk</div>
                                                </div>
                                            </div>
                                        </header>
                                        <main>
                                            <div class="row contacts">
                                                <div class="col invoice-to">
                                                    <div class="text-gray-light">INVOICE TO:</div>
                                                    <h2 class="to">{{$user->name}}</h2>
                                                    <div class="address">{{$user->address}}</div>
                                                    <div class="email"><a href="{{$user->email}}">{{$user->email}}</a>
                                                    </div>
                                                    <div class="address">{{$user->contact}}</div>
                                                </div>
                                                <div class="col invoice-details">
                                                    <h1 class="invoice-id">{{$bill_info->invoice_no}}</h1>
                                                    <div class="date">Date of
                                                        Invoice: {{\Carbon\Carbon::parse($bill_info->created_at)->format('Y-m-d')}}</div>
                                                    <div class="date">Due
                                                        Date: {{\Carbon\Carbon::parse($bill_info->created_at)->addDays(30)->format('Y-m-d')}}</div>
                                                    <div class="date">Month of <b>{{ucfirst($bill_info->month)}}</b>
                                                    </div>
                                                </div>
                                            </div>
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th class="text-left">DESCRIPTION</th>
                                                    <th class="text-right">TOTAL</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="no">01</td>
                                                    <td class="text-left">
                                                        <h3>
                                                            Water Usage bill for month of
                                                            <b>{{ucfirst($bill_info->month)}}</b>
                                                        </h3>
                                                    </td>
                                                    <td class="total">
                                                        LKR. {{number_format((float)$bill_info->charges_this_month, 2, '.', '')}}</td>
                                                </tr>
                                                @if($bill_info->due_previous_month AND $bill_info->due_previous_month>0)
                                                    <tr>
                                                        <td class="no">02</td>
                                                        <td class="text-left">
                                                            <h3>
                                                                Previous month due amount
                                                            </h3>
                                                        </td>
                                                        <td class="total">
                                                            LKR. {{number_format((float)$bill_info->due_previous_month, 2, '.', '')}}</td>
                                                    </tr>
                                                @endif
                                                @for($x=1;$x<4;$x++)
                                                    <tr>
                                                        <td class="no"></td>
                                                        <td class="text-left">
                                                        </td>
                                                        <td class="total"></td>
                                                    </tr>
                                                @endfor

                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="2">Total</td>
                                                    <td>
                                                        LKR. {{number_format((float)$bill_info->total_amount, 2, '.', '')}}</td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                            <br><br><br>
                                            <div class="notices">
                                                <div class="notice">Outdoor water use restrictions - all customers are
                                                    required to follow the year round
                                                    conservation rules listed on the back of this bill. Violators are
                                                    subject to fines in
                                                    accordance with Water Authority Rules.
                                                </div>
                                                <br>
                                                <div class="text-danger">NOTICE:</div>
                                                <div class="notice text-danger">Please make all payments withing 30 days
                                                    from invoice date.
                                                </div>
                                            </div>
                                        </main>
                                        <footer>
                                            Invoice was created via Water Bill System
                                            on {{\Carbon\Carbon::now()->format('Y-m-d H:i:s')}}
                                        </footer>
                                    </div>
                                    <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
                                    <div></div>
                                </div>
                                @if($bill_info->is_settle)
                                    <img src="{{asset('')}}image/paid.png" alt="" class="paid">
                                @endif
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">No bill available</p>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>

    @include('customer.paybutton')
@endsection

@section('extra-css')
    <style>
        .paid {
            width: 150px;
            position: absolute;
            top: 425px;
            left: 42%;
            opacity: 0.6;
        }

        .overdue_span {
            background: #d80a0a;
            border-radius: 20px;
            padding: 9px;
            color: #fff;
        }

        .owe_span {
            background: #38bf0d;
            border-radius: 20px;
            padding: 9px;
            color: #fff;
        }

        #menux .active a {
            color: #fff !important;
            font-weight: 600;
        }

        #menux a {
            color: #222;
            font-weight: 600;

        }

        /*invoice*/
        #invoice {
            padding: 0px 10px 30px;
        }

        .invoice {
            position: relative;
            background-color: #FFF;
            min-height: 680px;
            padding: 15px;
            border: 1px solid #ccc;
        }

        .invoice header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #3989c6
        }

        .invoice .company-details {
            text-align: right
        }

        .invoice .company-details .name {
            margin-top: 0;
            margin-bottom: 0
        }

        .invoice .contacts {
            margin-bottom: 20px
        }

        .invoice .invoice-to {
            text-align: left
        }

        .invoice .invoice-to .to {
            margin-top: 0;
            margin-bottom: 0
        }

        .invoice .invoice-details {
            text-align: right
        }

        .invoice .invoice-details .invoice-id {
            margin-top: 0;
            color: #3989c6
        }

        .invoice main {
            padding-bottom: 50px
        }

        .invoice main .thanks {
            margin-top: -100px;
            font-size: 2em;
            margin-bottom: 50px
        }

        .invoice main .notices {
            padding-left: 6px;
            border-left: 6px solid #3989c6
        }

        .invoice main .notices .notice {
            font-size: 1.2em
        }

        .invoice table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px
        }

        .invoice table td, .invoice table th {
            padding: 15px;
            background: #eee;
            border-bottom: 1px solid #fff
        }

        .invoice table th {
            white-space: nowrap;
            font-weight: 400;
            font-size: 16px
        }

        .invoice table td h3 {
            margin: 0;
            font-weight: 400;
            color: #3989c6;
            font-size: 1.2em
        }

        .invoice table .qty, .invoice table .total, .invoice table .unit {
            text-align: right;
            font-size: 1.2em
        }

        .invoice table .no {
            color: #fff;
            font-size: 1.6em;
            background: #3989c6
        }

        .invoice table .unit {
            background: #ddd
        }

        .invoice table .total {
            background: #3989c6;
            color: #fff
        }

        .invoice table tbody tr:last-child td {
            border: none
        }

        .invoice table tfoot td {
            background: 0 0;
            border-bottom: none;
            white-space: nowrap;
            text-align: right;
            padding: 10px 20px;
            font-size: 1.2em;
            border-top: 1px solid #aaa
        }

        .invoice table tfoot tr:first-child td {
            border-top: none
        }

        .invoice table tfoot tr:last-child td {
            color: #3989c6;
            font-size: 1.4em;
            border-top: 1px solid #3989c6
        }

        .invoice table tfoot tr td:first-child {
            border: none
        }

        .invoice footer {
            width: 100%;
            text-align: center;
            color: #777;
            border-top: 1px solid #aaa;
            padding: 8px 0
        }

        @media print {
            .invoice {
                font-size: 11px !important;
                overflow: hidden !important
            }

            .invoice footer {
                position: absolute;
                bottom: 10px;
                page-break-after: always
            }

            .invoice > div:last-child {
                page-break-before: always
            }
        }
    </style>
@endsection

@section('extra-js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
            $('#paynow_btn').click(function (e) {
                $('#payment_details_form').submit();
            });

        });
    </script>
@endsection
