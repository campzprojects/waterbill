-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2019 at 08:20 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `waterbill`
--

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` int(11) NOT NULL,
  `account_no` varchar(100) NOT NULL,
  `invoice_no` varchar(100) NOT NULL,
  `meter_no` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `month` varchar(100) NOT NULL,
  `current_reading` float DEFAULT NULL,
  `previous_reading` float DEFAULT NULL,
  `consumption` float DEFAULT NULL,
  `charges_this_month` float NOT NULL,
  `due_previous_month` float DEFAULT NULL,
  `total_amount` float NOT NULL,
  `balance` float DEFAULT NULL,
  `is_read` int(11) NOT NULL DEFAULT '0',
  `is_settle` int(11) NOT NULL DEFAULT '0',
  `red_notice` date DEFAULT NULL,
  `red_notice_due` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bills`
--

INSERT INTO `bills` (`id`, `account_no`, `invoice_no`, `meter_no`, `category`, `address`, `month`, `current_reading`, `previous_reading`, `consumption`, `charges_this_month`, `due_previous_month`, `total_amount`, `balance`, `is_read`, `is_settle`, `red_notice`, `red_notice_due`, `created_at`, `updated_at`) VALUES
(5, '123123', 'in_88230328', '12222222222222', 'industrial', 'sadcdc sdads', 'january', NULL, NULL, NULL, 500, 100, 600, NULL, 0, 1, NULL, NULL, '2019-03-28 01:21:21', '2019-03-30 14:45:19'),
(6, '1231234', 'in_88230328', '12222222222222', 'industrial', 'sadcdc sdads', 'may', NULL, NULL, NULL, 500, 100, 600, 500, 0, 1, '2019-03-29', '2019-04-01', '2019-03-29 01:21:21', '2019-03-30 14:49:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `account_no` varchar(100) NOT NULL,
  `payment_method` varchar(100) NOT NULL,
  `invoice_no` varchar(100) NOT NULL,
  `card_name` varchar(100) DEFAULT NULL,
  `card_last_digit` varchar(100) DEFAULT NULL,
  `total_amount` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `bill_id`, `customer_id`, `account_no`, `payment_method`, `invoice_no`, `card_name`, `card_last_digit`, `total_amount`, `created_at`, `updated_at`) VALUES
(1, 6, 1, '123123', 'Visa', 'in_88230328', 'salinda', '4242', 600, '2019-03-29 05:00:08', '2019-03-29 05:00:08'),
(2, 6, 1, '123123', 'Visa', 'in_8823032', 'salinda', '4242', 600, '2019-03-29 05:11:31', '2019-03-29 05:11:31'),
(3, 6, 1, '123123', 'Visa', 'in_88230328', 'salinda', '4242', 600, '2019-03-30 14:38:38', '2019-03-30 14:38:38'),
(4, 6, 1, '123123', 'Visa', 'in_88230328', NULL, '4242', 600, '2019-03-30 14:44:04', '2019-03-30 14:44:04'),
(5, 6, 1, '123123', 'Visa', 'in_88230328', 'salinda', '4242', 600, '2019-03-30 14:45:18', '2019-03-30 14:45:18'),
(6, 6, 2, '1231234', 'Visa', 'in_88230328', NULL, '4242', 600, '2019-03-30 14:48:27', '2019-03-30 14:48:27'),
(7, 6, 2, '1231234', 'Visa', 'in_88230328', NULL, '4242', 100, '2019-03-30 14:49:23', '2019-03-30 14:49:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nic` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `meter_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `due_previous_month` double DEFAULT NULL,
  `is_complete` int(11) NOT NULL DEFAULT '0',
  `role` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'customer',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `profile_image`, `name`, `email`, `password`, `remember_token`, `account_no`, `company`, `nic`, `contact`, `address`, `district`, `payment_method`, `post`, `status`, `meter_no`, `category`, `due_previous_month`, `is_complete`, `role`, `created_at`, `updated_at`) VALUES
(1, NULL, 'salinda jayawardana', 'salinda@gmail.com', '$2y$10$w9ZIHGp6tKKd3uDuFXFaneD43ALiYKS/3ZMBdBte9LyXQLt.xGIgu', 'mHIRqnZ5QRhNYnpNiCcgBCDpUCws0vPWYjXdfuw77kCJnGGb4FnkgZP65GOG', '123123', 'industrial', NULL, '254125563', 'sadcdc sdads', NULL, NULL, NULL, 1, '12222222222222', 'industrial', 0, 0, 'manager', NULL, '2019-03-30 14:38:38'),
(2, NULL, 'dhanushka jayawardana', 'ss@gmail.com', '$2y$10$w9ZIHGp6tKKd3uDuFXFaneD43ALiYKS/3ZMBdBte9LyXQLt.xGIgu', 'GCr6pZczJ8BCDxB3BMEOvPSm8iuMupqJ9k2TkG0RjPmRGaTs56J6CX2JI0cR', '1231234', 'industrial', '911480743v', '254125563', 'sadcdc sdads', NULL, NULL, NULL, 1, '12222222222222', 'industrial', 500, 0, 'customer', NULL, '2019-03-30 14:49:24'),
(6, NULL, 'dhanushka jayawardana', 'ss@gmail.com2', '$2y$10$w9ZIHGp6tKKd3uDuFXFaneD43ALiYKS/3ZMBdBte9LyXQLt.xGIgu', 'GCr6pZczJ8BCDxB3BMEOvPSm8iuMupqJ9k2TkG0RjPmRGaTs56J6CX2JI0cR', '123123', 'industrial', '911480743v', '254125563', 'sadcdc sdads', NULL, NULL, NULL, 1, '12222222222222', 'industrial', 600, 0, 'bill-officer', NULL, '2019-03-30 10:28:07'),
(10, NULL, 'eeeeeeeee', 'assaa@gmail.com', '$2y$10$v8Y3N0QXo/1fI37/Xi7NJOKMmbyg8fNCStS7tVGaGcizMI.xOZfMi', NULL, NULL, NULL, '444444444444', '444444444444', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 'bill-officer', '2019-03-30 11:07:28', '2019-03-30 11:07:28'),
(12, NULL, 'krish', 'krish@gmail.com', '$2y$10$gZdCrAbt9q4irF5iN5YPr.sIuaK2FBtAvyR6E0MAYMrcml9m6hgma', 'BccM2v6KghwQlzaKD3o8iY4UuuCEw2fxbn58k5EmJRfj7MMeW5PRQXI6Zhgl', NULL, NULL, '123123123', '123123123', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 'customer-support', '2019-03-30 11:21:52', '2019-03-30 11:21:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
